﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Lab1_rybakowski.Rest.Models;

namespace Lab1_rybakowski.Rest.Context
{
    public class AzureDbContext : DbContext
    {
        public AzureDbContext(DbContextOptions<AzureDbContext> options) : base(options)
        {

        }

        protected AzureDbContext()
        {

        }

        public DbSet<Person> People { get; set; }
    }
}
